
#include <StdAfx.h>
#include "PhatSDK.h"
#include "SkillChecks.h"

double GetSkillChance(int skill, int difficulty)
{
	double chance = 1.0 - (1.0 / (1.0 + exp(0.03 * (skill - difficulty))));

	return min(1.0, max(0.0, chance));
}

double GetAppraisalSkillChance(int skill, int difficulty)
{
	double chance = 1.0 - (1.0 / (1.0 + exp(0.05 * (skill - difficulty))));

	return min(1.0, max(0.0, chance));
}

double GetMagicSkillChance(int skill, int difficulty)
{
	double chance = 1.0 - (1.0 / (1.0 + exp(0.07 * (skill - difficulty))));

	return min(1.0, max(0.0, chance));
}

int GetManaCost(int skill, int difficulty, int manaCost, int manaConversion)
{
	if (!manaConversion)
		return manaCost;

	// Dropping diff by half as Specced ManaC is only 48 with starter Aug so 50 at level 1 means no bonus
	//   easiest change without having to create two different formulas to try to emulate retail
	double successChance = GetSkillChance(manaConversion, static_cast<int>(difficulty / 2));
	float roll = Random::RollDice(0, 1);

	// Luck lowers the roll value to give better outcome
	// e.g. successChance = .83 & roll = .71 would still provide some savings.  
	//   but a luck roll of .19 will lower that .71 to .13 so the caster would
	//   receive a 60% reduction in mana cost.  without the luck roll, 12%
	//   so players will always have a level of "luck" in manacost if they make skill checks
	float luck = Random::RollDice(0, 1);

	if (roll <= successChance)
	{
		manaCost *= (1.0f - (successChance - (roll * luck)));  
	}

	// above seems to give a good middle of the range
	// seen in pcaps for mana usage for low level chars
	// bug still need a way to give a better reduction for the "lucky"

	// save some calc time if already at 1 mana cost
	if (manaCost > 1)
	{
		successChance = GetSkillChance(manaConversion, difficulty);
		roll = Random::RollDice(0, 1);

		if (roll <= successChance)
		{
			manaCost *= (1.0f - (successChance - (roll * luck)));
		}
	}

	return max(manaCost, 1);
}

bool GenericSkillCheck(int offense, int defense)
{
	double chance = GetSkillChance(offense, defense);

	if (Random::RollDice(0.0, 1.0) <= chance)
	{
		// succeeded
		return true;
	}

	// failed
	return false;
}

bool AppraisalSkillCheck(int offense, int defense)
{
	double chance = GetAppraisalSkillChance(offense, defense);

	if (Random::RollDice(0.0, 1.0) <= chance)
	{
		// succeeded
		return true;
	}

	// failed
	return false;
}

bool TryMagicResist(int offense, int defense)
{
	return !GenericSkillCheck(offense, defense);
}

bool TryMeleeEvade(int offense, int defense)
{
	return !GenericSkillCheck(offense, defense);
}

bool TryMissileEvade(int offense, int defense)
{
	return !GenericSkillCheck(offense, defense);
}
